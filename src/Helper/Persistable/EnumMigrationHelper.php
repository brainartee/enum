<?php

namespace Brainart\Enum\Helper\Persistable;

use Brainart\Enum\Model\Persistable\Doctrine\DoctrineEnum;

/**
 * Class EnumMigrationHelper
 *
 * Class to help migrations. Example:
 *  <code>
 *  $itemsData = [
 *      'CONTACT_PERSON' => 'Kontaktisik',
 *      'CONTACT_PERSON2' => 'Kontaktisik2'
 *  ];
 *
 *  (new EnumMigrationHelper(XPersonRelationTypeEnum::class))
 *      ->persistItems($itemsData);
 *  </code>
 *
 */
class EnumMigrationHelper
{
    protected $enumClass;
    protected $enum;

    public function __construct($enumClass)
    {
        $this->enumClass = $enumClass;
        $this->enum = new $enumClass;
    }

    /**
     * Array of items to persist
     *
     * @param array $itemsData E.g ['id1' => 'name1', 'id2' => 'name2', ...]
     * @return EnumMigrationHelper
     * @todo Optimize
     */
    public function persistItems(array $itemsData): self
    {
        /** @var DoctrineEnum $enum */
        $enum = $this->enum;
        $itemClass = ($this->enumClass)::getItemClass();

        $items = [];
        foreach ($itemsData as $id => $itemData) {
            $items[] = new $itemClass($enum, $id, $itemData);
        }

        $enum->persistMany($items);

        return $this;
    }

    /**
     * Removes all items
     * @todo Optimize
     */
    public function removeItems()
    {
        /** @var DoctrineEnum $enum */
        $enum = $this->enum;
        $itemClass = ($this->enumClass)::getItemClass();

        $enum->removeMany(true);

        return $this;
    }
}
