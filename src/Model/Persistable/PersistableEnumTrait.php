<?php

namespace Brainart\Enum\Model\Persistable;

trait PersistableEnumTrait
{
    /**
     * @var mixed Database handler/connection object
     */
    protected static $db = null;

    /**
     * Sets database handler
     * @param mixed $db
     */
    public static function setDb($db): void
    {
        static::$db = $db;
    }

    /**
     * Returns database handler
     * @return mixed
     */
    public static function getDb()
    {
        return static::$db;
    }
}
