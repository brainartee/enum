<?php

namespace Brainart\Enum\Model\Persistable\Doctrine;

use \Brainart\Enum\Model\Persistable\PersistableEnumTrait;
use \Brainart\Enum\Model\Enum;
use \Doctrine\Persistence\ManagerRegistry;
use \Doctrine\ORM\EntityManager;
use \Doctrine\ORM\QueryBuilder;

abstract class DoctrineEnum extends Enum
{
    use PersistableEnumTrait {
        setDb as setDbBase;
        getDb as getDbBase;
    }

    protected static $items = null;

    public static function setDb(ManagerRegistry $db): void
    {
        self::setDbBase($db);
    }

    public static function getDb(): ManagerRegistry
    {
        return self::getDbBase();
    }

    /**
     * Returns all enum items
     * @param bool $plain If true, returns plain id:name array. If false, returns Item objects
     * @return DoctrineItem[]|string[]
     * @todo Merge queries in self::all() and self::get()
     */
    public function all(bool $plain = false): array
    {
        /** @var EntityManager $em */
        $em = self::getDb()->getManager();
        $itemClass = static::getItemClass();

        // TODO: merge queries in all() and get()
        $query = $em->getRepository($itemClass)
            ->createQueryBuilder('i', 'i.id')
            ->getQuery();

        if ($plain) {
            $items = $query->getArrayResult();
            // NOTE array_column without 3rd param resets the keys
            $items = array_column($items, 'name', 'id');
        } else {
            $items = $query->getResult();
            // TODO: if taken from Doctrine cache, enum has already been set
            foreach ($items as $item) {
                if (!$item->getEnum()) {
                    $item->setEnum($this);
                }
            }
        }

        return $items;
    }

    /**
     * @param string $id
     * @param bool $plain
     * @return DoctrineItem|mixed|string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @todo Merge queries in self::all() and self::get()
     */
    public function get($id, bool $plain = false)
    {
        /** @var EntityManager $em */
        $em = self::getDb()->getManager();
        $itemClass = static::getItemClass();

        if ($plain) {
            $res = $em->find($itemClass, $id)->getName();
        } else {
            /** @var DoctrineItem $res */
            $res = $em->find($itemClass, $id);
            if (!$res->getEnum()) {
                $res->setEnum($this);
            }
        }

        return $res;
    }

    /**
     * Stores single item into database
     */
    public function persist(DoctrineItem $item): self
    {
        /** @var EntityManager $em */
        $em = self::getDb()->getManager();
        $em->persist($item);
        $em->flush();

        return $this;
    }

    /**
     * Stores multiple items into database
     * @param DoctrineItem[] $items
     * @return DoctrineEnum
     */
    public function persistMany(array $items): self
    {
        /** @var EntityManager $em */
        $em = self::getDb()->getManager();
        foreach ($items as $item) {
            $em->persist($item);
        }
        $em->flush();

        return $this;
    }

    /**
     * Removes item from database
     * @param DoctrineItem $item
     * @return DoctrineEnum
     */
    public function remove(DoctrineItem $item): self
    {
        /** @var EntityManager $em */
        $em = self::getDb()->getManager();
        $em->remove($item);
        $em->flush();

        return $this;
    }

    /**
     * Removes multiple items from database
     * @param DoctrineItem[]|boolean $items If true, removes all
     * @return DoctrineEnum
     * @todo Needs optimization
     */
    public function removeMany($items): self
    {
        /** @var EntityManager $em */
        $em = self::getDb()->getManager();
        $itemClass = static::getItemClass();

        if ($items === true) {
            /** @var QueryBuilder $qb */
            $qb = $em->createQueryBuilder();
            $qb->delete($itemClass)
                ->getQuery()
                ->execute();
        } else {
            foreach ($items as $item) {
                $em->remove($item);
            }
        }

        $em->flush();

        return $this;
    }

    /**
     * @return string
     */
    public static function getItemClass(): string
    {
        // NOTE: child must implement DoctrineItem
        throw new \RuntimeException('Enum Item class not defined, this needs to be implemented in child class');
    }
}
