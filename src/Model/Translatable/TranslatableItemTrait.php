<?php

namespace Brainart\Enum\Model\Translatable;

/**
 * Trait TranslatableItemTrait
 * 
 * @property string $name
 */
trait TranslatableItemTrait
{
    /**
     * Returns translation for given $id
     * @param string $token Translation token, e.g 'FADYYT.THIS'
     * @param string|null $locale
     * @return string
     */
    abstract protected function translate($token, ?string $locale = null): string;

    /**
     * Returns translated name
     * @param string|null $locale
     * @return string
     */
    public function getName(?string $locale = null): string
    {
        return $this->translate($this->name, $locale);
    }
}
