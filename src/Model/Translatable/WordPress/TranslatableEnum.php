<?php

namespace Brainart\Enum\Model\Translatable\WordPress;

use Brainart\Enum\Model\Translatable\TranslatableEnumTrait;
use Brainart\Enum\Model\Enum;

/**
 * @method TranslatableItem|string get($id, $plain = false)
 * @method TranslatableItem[]|string[] all($plain = false)
 * @method string[] allFlipped()
 */
class TranslatableEnum extends Enum
{
    use TranslatableEnumTrait {
        setTranslator as setTranslatorBase;
        getTranslator as getTranslatorBase;
    }

    protected static $translationDomain = 'default';

//    protected static $items = [
//        'FOO' => 'translation_token'
//    ];

    public static function setTranslator($translator): void
    {
        throw new \InvalidArgumentException(__METHOD__ . ' not implemented because there is no "Translator" object in WP');
    }

    public static function getTranslator()
    {
        throw new \InvalidArgumentException(__METHOD__ . ' not implemented because there is no "Translator" object in WP');
    }

    public static function getItemClass(): string
    {
        return TranslatableItem::class;
    }

    public static function getTranslationDomain(): string
    {
        return static::$translationDomain;
    }
}
