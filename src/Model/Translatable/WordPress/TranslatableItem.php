<?php

namespace Brainart\Enum\Model\Translatable\WordPress;

use Brainart\Enum\Model\Item;
use Brainart\Enum\Model\Translatable\TranslatableItemTrait;
use http\Exception\InvalidArgumentException;

class TranslatableItem extends Item
{
    use TranslatableItemTrait;

    /**
     * @var TranslatableEnum
     */
    protected $enum;

    /**
     * Item constructor.
     * @param TranslatableEnum $enum
     * @param mixed $id
     * @param string|array $data If non-array, then name. If array, then key-value pairs
     */
    public function __construct(TranslatableEnum $enum, $id, $data)
    {
        parent::__construct($enum, $id, $data);
    }

    protected function translate($token, ?string $locale = null): string
    {
        if ($locale) {
            throw new InvalidArgumentException(sprintf('This is WordPress and we cannot switch locales runtime'));
        }

        // TODO: translator's 2nd parameter "domain" should be fed from Enum or smth
        return __($token, ($this->enum)::getTranslationDomain());
    }
}
