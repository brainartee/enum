<?php

namespace Brainart\Enum\Model\Translatable;

trait TranslatableEnumTrait
{
    /**
     * @var mixed Translator object
     */
    protected static $translator = null;

    /**
     * Sets translator object
     * @param mixed $translator
     */
    public static function setTranslator($translator): void
    {
        static::$translator = $translator;
    }

    /**
     * Returns translator object
     */
    public static function getTranslator()
    {
        return static::$translator;
    }
}
