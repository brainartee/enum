<?php

namespace Brainart\Enum\Model\Translatable\Symfony;

use Brainart\Enum\Model\Translatable\TranslatableEnumTrait;
use Brainart\Enum\Model\Enum;

/**
 * @method TranslatableItem|string get($id, $plain = false)
 * @method TranslatableItem[]|string[] all($plain = false)
 * @method string[] allFlipped()
 */
class TranslatableEnum extends Enum
{
    use TranslatableEnumTrait {
        setTranslator as setTranslatorBase;
        getTranslator as getTranslatorBase;
    }

    protected static $items = [
        'FOO' => 'enum.translatable.foo'
    ];

    /**
     * @param \Symfony\Component\Translation\TranslatorInterface|\Symfony\Contracts\Translation\TranslatorInterface $translator
     */
    public static function setTranslator($translator): void
    {
        self::setTranslatorBase($translator);
    }

    /**
     * @return \Symfony\Component\Translation\TranslatorInterface|\Symfony\Contracts\Translation\TranslatorInterface $translator
     */
    public static function getTranslator()
    {
        return static::getTranslatorBase();
    }

    public static function getItemClass(): string
    {
        return TranslatableItem::class;
    }
}
