<?php

namespace Brainart\Enum\Model\Translatable\Symfony;

use Brainart\Enum\Model\Item;
use Brainart\Enum\Model\Translatable\TranslatableItemTrait;

class TranslatableItem extends Item
{
    use TranslatableItemTrait;

    /**
     * @var TranslatableEnum
     */
    protected $enum;

    /**
     * Item constructor.
     * @param TranslatableEnum $enum
     * @param mixed $id
     * @param string|array $data If non-array, then name. If array, then key-value pairs
     */
    public function __construct(TranslatableEnum $enum, $id, $data)
    {
        parent::__construct($enum, $id, $data);
    }

    protected function translate($token, ?string $locale = null): string
    {
        $translator = ($this->enum)::getTranslator();
        return $translator->trans($token, [], null, $locale);
    }
}
