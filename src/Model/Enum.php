<?php

namespace Brainart\Enum\Model;

class Enum implements \IteratorAggregate
{
    protected static $items = [
        'FOO' => 'bar'
    ];

    /**
     * Returns all enum items
     * @param bool $plain If true, returns plain id:name array. If false, returns Item objects
     * @return Item[]|string[]
     */
    public function all(bool $plain = false): array
    {
        $items = [];
        foreach (static::$items as $id => $name) {
            $items[$id] = $this->get($id, $plain);
        }

        return $items;
    }

    /**
     * Returns all enum items flipped
     * @return string[]
     */
    public function allFlipped(): array
    {
        $items = [];
        foreach (static::$items as $id => $name) {
            $items[$this->get($id, true)] = $id;
        }

        return $items;
    }

    /**
     * @param mixed $id
     * @param bool $plain
     * @return Item|string
     */
    public function get($id, bool $plain = false)
    {
        $itemClass = static::getItemClass();
        /** @var Item $res */
        $res = new $itemClass($this, $id, static::$items[$id]);
        if ($plain) {
            $res = $res->getName();
        }

        return $res;
    }

    /**
     * @param mixed $id
     * @return bool
     */
    public function has($id): bool
    {
        return array_key_exists($id, static::$items);
    }

    /**
     * @return \ArrayIterator|\Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->all());
    }

    protected static function getItemClass(): string
    {
        return Item::class;
    }
}
