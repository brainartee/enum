<?php

namespace Brainart\Enum\Model;

class Item
{
    /**
     * @var Enum
     */
    protected $enum;
    /**
     * @var mixed
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;

    /**
     * Item constructor.
     * @param Enum $enum
     * @param mixed $id
     * @param string|array $data If non-array, then name. If array, then key-value pairs
     */
    public function __construct(Enum $enum, $id, $data)
    {
        $this->enum = $enum;
        $this->id = $id;
        if (!is_array($data)) {
            $this->name = $data;
        } else {
            foreach ($data as $k => $datum) {
                $this->$k = $datum;
            }
        }
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getEnum(): Enum
    {
        return $this->enum;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
