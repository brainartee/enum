# Enum Library

Enum (or classifier) is a set of predefined values, e.g "Color":

* red
* green
* blue

Simplest enum has id and name, and it requires 2 classes:

* enum (or "list")
* enum item (or "list item")

## Setup

1) Add repository url to your composer:
```
composer config repositories."brainart/enum" vcs https://bitbucket.org/brainartee/enum.git
```

2) Require enum:
```
composer require "brainart/enum"
```

4) Work

## Defining enums

### Simple enum

Simple id=name enum does not require more than defining enum class.

```
class Color extends \Brainart\Enum\Model\Enum
{
    const RED = 'RED';
    const GREEN = 'GREEN';
    const BLUE = 'BLUE';
    
    protected static $items = [
        self::RED => 'red',
        self::GREEN => 'green',
        self::BLUE => 'blue'
    ];
}
```

Here value constants are optional. These are helpful if you need special treatment for some, e.g:
```
if ($color === Color::RED) {
    echo "ERROR!";
}
```

## Usage

Get single item:
```
$colors = (new Color);
echo $colors->get(Color::RED, true);
// "red"
echo $colors->get(Color::RED);
// instance of \Brainart\Enum\Model\Item...
```

Get all items:
```
$colors = (new Color);
print_r($colors->all(true));
// red, green, blue
print_r($colors->all());
// array of \Brainart\Enum\Model\Item...
```

Traverse:
```php
$colors = (new Color);
foreach ($colors->all() as $id => $color)...
// or simpler
foreach ($colors as $id => $color)...
```

## Etc

* [Symfony translatable enum](docs/symfony-translatable.md)
* [WordPress translatable enum](docs/wp-translatable.md)
* [Persisted enum](docs/persisted.md)
* [Custom enum](docs/custom.md)
