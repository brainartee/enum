### Custom enum

You can use Enum in anywhere.

Joomla's translatable enum example:

```
class JoomlaTranslatableEnum extends \Brainart\Enum\Model\Enum
{
    use \Brainart\Enum\Model\Translatable\TranslatableEnumTrait;
    
    // no setTranslator, getTranslator overriding needed as there is no "translator object" in Joomla
    
    protected static function getItemClass(): string
    {
        return JoomlaTranslatableItem::class;
    }
}

class JoomlaTranslatableItem extends \Brainart\Enum\Model\Item
{
    use \Brainart\Enum\Model\Translatable\Symfony\TranslatableItem;

    /**
     * @var JoomlaTranslatableEnum
     */
    protected $enum;

    protected function translate($id, $locale = null): string
    {
        return \JText::_($id, $locale);
    }
}
```

Define enums:
```
class UserStatusEnum extends JoomlaTranslatableEnum
{
    protected static $items = [
        'ACTIVE' => 'COM_HEALTHPROFILE_USER_STATUS_ACTIVE',
        'DEACTIVE' => 'COM_HEALTHPROFILE_USER_STATUS_DEACTIVE',
        'DELETED' => 'COM_HEALTHPROFILE_USER_STATUS_DELETED'
    ];
}
```
