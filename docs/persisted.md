# Persisted Enums

Which mean enums stored in database. Currently Doctrine is supported.

1a) If you are developing on Symfony, you can install [brainart/enum-bundle](https://bitbucket.org/brainartee/enum-bundle).

1b) If not, write this line somewhere:
```
/** @var \Doctrine\Persistence\ManagerRegistry $doctrine */
$doctrine = ...;
\Brainart\Enum\Model\Persistable\Doctrine\DoctrineEnum::setDb($doctrine);
```

2) Create base classes - Enum and Item classes:

```
class XPersonRelationTypeEnum extends \Brainart\Enum\Model\Persistable\Doctrine\DoctrineEnum
{
    // not required
    //const CONTACT_PERSON = 'CONTACT_PERSON';

    public static function getItemClass(): string
    {
        return XPersonRelationType::class;
    }
}

/**
 * @ORM\Entity
 * @ORM\Table(name="x_person_relation_type")
 */
class XPersonRelationType extends \Brainart\Enum\Model\Persistable\Doctrine\DoctrineItem
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=30, unique=true)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;
}
```

3) Create migration which creates SQL for table creation:
```
$ bin/console doctrine:migrations:diff
```

4) Alter migration to import initial data:
```
$itemsData = [
    'CONTACT_PERSON' => 'Kontaktisik',
];

(new \Brainart\Enum\Helper\Doctrine\EnumMigrationHelper(XPersonRelationTypeEnum::class))
    ->persistItems($itemsData);
```
