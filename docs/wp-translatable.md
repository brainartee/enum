# WordPress translatable enum

You can make Enum translatable via WordPress Translator.

1) Extend WordPress\TranslatorEnum instead of Enum
2) Set translation domain

```php
<?php
class Color extends \Brainart\Enum\Model\Translatable\WordPress\TranslatableEnum
{
    const RED = 'RED';
    const GREEN = 'GREEN';
    const BLUE = 'BLUE';

    protected static $translationDomain = 'legend';

    protected static $items = [
        self::RED => 'color.red', // translations
        self::GREEN => 'color.green',
        self::BLUE => 'color.blue'
    ];
}
```
