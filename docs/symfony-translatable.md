# Symfony Translatable enum

You can make Enum translatable via Symfony's Translator.

Extend Symfony\TranslatorEnum instead of Enum:

```php
<?php
class Color extends \Brainart\Enum\Model\Translatable\Symfony\TranslatableEnum
{
    const RED = 'RED';
    const GREEN = 'GREEN';
    const BLUE = 'BLUE';

    protected static $items = [
        self::RED => 'enum.color.red', // translations
        self::GREEN => 'enum.color.green',
        self::BLUE => 'enum.color.blue'
    ];
}
```

